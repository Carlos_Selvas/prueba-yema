from django.test import TestCase
from api.views import _busca_medico, _busca_medico_por_nombre_completo, _busca_medico_por_apellido, \
    _busca_medico_2_nombres, _busca_medico_mas_de_2_nombres
from pediatria.models import Medico


class TestFuncionesAPI(TestCase):
    def setUp(self):
        self.medico1 = Medico.objects.create(nombre='Manuel', apellido='Martínez', email='mm@pediatria.com')
        self.medico2 = Medico.objects.create(nombre='Nico', apellido='Narvarte', email='nn@pediatria.com')
        self.medico3 = Medico.objects.create(nombre='Osvaldo', apellido='Ozuna Oropeza', email='oo@pediatria.com')
        self.medico4 = Medico.objects.create(nombre='Pedro', apellido='Páramo', email='pp@pediatria.com')

    def test_busca_medico(self):
        """
        Debe encontrar por correo, por nombre completo o por apellidos

        :return:
        """
        prueba1_1 = _busca_medico('mm@pediatria.com')
        prueba1_2 = _busca_medico('Nico Narvarte')
        prueba1_3 = _busca_medico('Martínez')
        prueba1_4 = _busca_medico('Ozuna Oropeza')
        self.assertIsNotNone(prueba1_1)
        self.assertIsNotNone(prueba1_2)
        self.assertIsNotNone(prueba1_3)
        self.assertIsNotNone(prueba1_4)

    def test_busca_medico_por_nombre_completo(self):
        """
        Si tenemos sólo el apellido, o el nombre completo del médico

        :return:
        """
        prueba2_1 = _busca_medico_por_nombre_completo('Martínez')
        prueba2_2 = _busca_medico_por_nombre_completo('Osvaldo Ozuna Oropeza')
        prueba2_3 = _busca_medico_por_nombre_completo('Nico Narvarte')
        self.assertIsNotNone(prueba2_1)
        self.assertIsNotNone(prueba2_2)
        self.assertIsNotNone(prueba2_3)

    def test_busca_medico_por_apellido(self):
        """
        Si sólo tenemos el apellido del médico

        :return:
        """
        prueba3_1 = _busca_medico_por_apellido('Páramo')
        prueba3_2 = _busca_medico_por_apellido('Ozuna Oropeza')
        self.assertIsNotNone(prueba3_1)
        self.assertIsNotNone(prueba3_2)

    def test_busca_medico_2_nombres(self):
        """
        Si contiene sólo dos nombres, pueden ser nombre y apellido o dos apellidos

        :return:
        """
        prueba4_1 = _busca_medico_2_nombres('Nico Narvarte')
        prueba4_2 = _busca_medico_2_nombres('Ozuna Oropeza')
        self.assertIsNotNone(prueba4_1)
        self.assertIsNotNone(prueba4_2)

    def test_busca_medico_mas_de_2_nombres(self):
        """
        Si contiene más de 2 nombres en la asignación del médico

        :return:
        """
        prueba5 = _busca_medico_mas_de_2_nombres('Osvaldo Ozuna Oropeza')
        self.assertIsNotNone(prueba5)
