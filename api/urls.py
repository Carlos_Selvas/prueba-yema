from django.conf.urls import url, include
from api.views import agenda_cita

urlpatterns = [
    url(r'^agenda-cita/$', agenda_cita),
]
