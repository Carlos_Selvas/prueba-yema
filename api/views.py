from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.serializers import ValidationError
from django.utils.translation import gettext as _

from hospital.models import Paciente
from pediatria.models import Medico
from pediatria.serializers import CitaSerializer


@api_view(['POST'])
def agenda_cita(request):
    # TODO: Pruebas unitarias
    # Lo voy a restringir a sólo staff, la idea es que un asistente (o un usuario API con permiso de staff) esté
    # usando un endpoint para este servicio
    if not request.user.is_staff:
        return Response(status=403)
    # Busca al médico relacionado; si no lo encuentra o encuentra más de uno, debe regresar error
    medico = _busca_medico(request.data['medico'])
    try:
        paciente = Paciente.objects.get(usuario__email=request.data['email_paciente'])
    except:
        raise ValidationError({
            _('email_paciente'): _('Por favor verifica los datos del paciente')
        })
    if not medico:
        raise ValidationError({
            _('medico'): _('No hemos podido encontrar un médico con esos datos, por favor especifica más.')
        })

    serializer_data = request.data.copy()
    serializer_data['medico'] = medico.pk
    serializer_data['paciente'] = paciente.pk
    serializer = CitaSerializer(data=serializer_data)
    if serializer.is_valid():
        try:
            serializer.save()
        except Exception as e:
            print(e)
            raise ValidationError({
                _('inicio_cita'): _('Este horario no está disponible, intenta con una fecha u hora distintas')
            })
        serializer_data_response = serializer.data.copy()
        serializer_data_response['medico'] = str(Medico.objects.get(id=serializer.data['medico']))
        serializer_data_response['paciente'] = str(Paciente.objects.get(usuario_id=serializer.data['paciente']))

        return Response(serializer_data_response, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def _busca_medico(medico_string):
    # TODO: Pruebas unitaria
    # Si viene el correo del médico es más fácil
    try:
        return Medico.objects.get(email__iexact=medico_string)
    except:
        return _busca_medico_por_nombre_completo(medico_string)


def _busca_medico_por_nombre_completo(medico_string):
    # TODO: Prueba unitaria
    elementos_nombre = medico_string.split(' ')

    if len(elementos_nombre) > 2:
        # Probablemente nombre o apellido se componen de más de una palabra
        return _busca_medico_mas_de_2_nombres(medico_string)
    elif len(elementos_nombre) == 2:
        return _busca_medico_2_nombres(medico_string)
        # return _busca_medico_mas_de_2_nombres(medico_string)
    else:
        # Si sólo viene una palabra, podría ser el apellido, pero sólo regresaremos resultado si existe sólo un médico
        # que tenga ese apellido
        return _busca_medico_por_apellido(medico_string)


def _busca_medico_por_apellido(medico_string):
    # TODO: Prueba unitaria
    try:
        return Medico.objects.get(apellido__iexact=medico_string)
    except:
        return


def _busca_medico_2_nombres(medico_string):
    # TODO: Pruebas unitarias
    elementos_nombre = medico_string.split(' ')
    # Probablemente vienen un nombre y un apellido, pero no sabemos en qué orden
    opcion_1 = Medico.objects.filter(nombre__iexact=elementos_nombre[0], apellido__iexact=elementos_nombre[1]).last()
    opcion_2 = Medico.objects.filter(nombre__iexact=elementos_nombre[1], apellido__iexact=elementos_nombre[0]).last()
    # Si ambos existen, regresa error. Si existe sólo uno de los dos, lo regresa como resultado. Si no existe
    # ninguno de los dos continúa
    if opcion_1 and opcion_2:
        return
    elif opcion_1 or opcion_2:
        return opcion_1 if opcion_1 else opcion_2
    else:
        # Pueden venir dos nombres o dos apellidos; sólo si vienen dos apellidos regresamos un resultado
        return _busca_medico_por_apellido(medico_string)


def _busca_medico_mas_de_2_nombres(medico_string):
    # TODO: Pruebas unitarias
    # Primero intenta buscar a un médico que tenga más de dos apellidos (p.e. De la Rosa); si hay más de un
    # resultado ( ¬¬) regresa error
    if _busca_medico_por_apellido(medico_string):
        return _busca_medico_por_apellido(medico_string)
    else:
        # Ya es una combinación de nombres y apellidos
        elementos = medico_string.split(' ')
        for n in range(0, len(elementos)):
            nombres = elementos[0:n]
            apellidos = elementos[n:len(elementos)]
            try:
                return Medico.objects.get(nombre=' '.join(nombres), apellido=' '.join(apellidos))
            except:
                pass
        return None
