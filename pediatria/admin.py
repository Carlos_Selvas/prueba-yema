from django.contrib import admin
from pediatria.models import Medico, Cita
from pediatria.funciones_auxiliares import envia_mails_cita_creada


def envia_correo(modeladmin, request, queryset):
    envia_mails_cita_creada(queryset.last())


envia_correo.short_description = "Envía por correo esta cita"


class CitaAdmin(admin.ModelAdmin):
    list_filter = ['medico']
    search_fields = ['medico', 'paciente', 'inicio_cita', 'comentario']
    fields = ['medico', 'paciente', 'inicio_cita', 'comentario']
    list_display = ['medico', 'paciente', 'inicio_cita', 'comentario']
    actions = [envia_correo]

    class Meta:
        model = Cita


admin.site.register(Medico)
admin.site.register(Cita, CitaAdmin)
