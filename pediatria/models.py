from django.db import models
from django.contrib.auth.models import User
from hospital.models import Paciente
from pediatria.funciones_auxiliares import envia_mails_cita_creada


class Medico(models.Model):
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    email = models.EmailField()

    def __str__(self):
        return self.nombre + ' ' + self.apellido

    class Meta:
        verbose_name = 'Médico'


class Cita(models.Model):
    medico = models.ForeignKey(Medico, on_delete=models.CASCADE, verbose_name="Médico")
    paciente = models.ForeignKey(Paciente, on_delete=models.CASCADE, verbose_name='Paciente')
    comentario = models.TextField(verbose_name="Comentario", max_length=1000, blank=True)
    inicio_cita = models.DateTimeField(default=None)

    def __str__(self):
        return self.inicio_cita.strftime('%Y-%m-%d %H:%M')

    def save(self, *args, **kwargs):
        if not self.pk:
            # Cuando se genere la cita envía un correo tanto al médico como al paciente con los datos de la cita
            envia_mails_cita_creada(self)
        super(Cita, self).save(*args, **kwargs)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['medico', 'inicio_cita'], name='asignacion_unica_medico'),
            models.UniqueConstraint(fields=['paciente', 'inicio_cita'], name='asignacion_unica_paciente'),
        ]
