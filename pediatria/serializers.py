from rest_framework import serializers

from pediatria.models import Cita


class CitaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cita
        fields = ['medico', 'paciente', 'comentario', 'inicio_cita']
