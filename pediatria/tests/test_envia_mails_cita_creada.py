from django.test import TestCase
from django.core import mail
from pediatria.funciones_auxiliares import envia_mails_cita_creada
from pediatria.models import Medico, Cita
from hospital.models import Paciente
from django.contrib.auth.models import User
from datetime import datetime as dt


class TestEnvia_mails_cita_creada(TestCase):
    def setUp(self):
        extras = {
            'first_name': 'Paciente',
            'last_name': 'Prueba'
        }
        self.usuario = User.objects.create_user('paciente@correo.com', 'paciente@correo.com', 'password', **extras)

        self.medico = Medico.objects.create(nombre='Médico', apellido='Prueba', email='medico1@pediatria.com')
        self.paciente = Paciente.objects.create(usuario=self.usuario, telefono='5555555555')
        self.cita = Cita.objects.create(
            medico=self.medico,
            paciente=self.paciente,
            comentario='Comentario',
            inicio_cita=dt.now()
        )

    def test_envia_mails_cita_creada(self):
        """
        Prueba que el correo haya salido de la bandeja de entrada y que coincida con el subject del método principal

        :return:
        """
        envia_mails_cita_creada(self.cita)
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[1].subject, 'Nueva cita creada')
