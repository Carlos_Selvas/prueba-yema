from django.core.mail import send_mail
from django.utils.translation import ugettext_lazy as _


def envia_mails_cita_creada(cita_object):
    """
    Método para enviar tanto al médico como al paciente el correo con información de la cita

    :param cita_object: el objeto Cita que se enviará
    :return: None
    """
    # TODO: Carga los datos de la cita (como invite si me da tiempo)
    nombre_paciente = cita_object.paciente.usuario.first_name + ' ' + cita_object.paciente.usuario.last_name

    mensaje = str(_('Se ha generado una nueva cita.\n')) + str(_('Paciente: ')) + nombre_paciente + '\n' + str(_('Médico: ')) + str(
        cita_object.medico) + '\n' + str(_('Fecha y hora: ')) + str(cita_object)
    send_mail(str(_('Nueva cita creada')), mensaje, 'citas@pediatria.com',
              [cita_object.medico.email, cita_object.paciente.usuario.email], True)


def medicos_tuple():
    """
    Regresa todos los Médicos como tuple para usarlos en los formularios

    :return:
    """
    from pediatria.models import Medico
    from django.db.models.functions import Concat
    from django.db.models import Value
    check = Medico.objects.annotate(
        full_name=Concat('nombre', Value(' '),
                         'apellido')).values_list('id', 'full_name')
    return check
