from django.test import TestCase, Client


class TestVistasAccesibles(TestCase):
    """
    Esta clase es sólo para probar que las vistas están accesibles, TODO: completar

    """
    def setUp(self):
        self.c = Client()

    def test_inicio(self):
        check = self.c.get('/')
        self.assertTrue(check.status_code == 200)

    def test_registro(self):
        check = self.c.get('/registro/')
        self.assertTrue(check.status_code == 200)

    def test_accede(self):
        check = self.c.get('/accede/')
        self.assertTrue(check.status_code == 200)

    def test_mis_citas(self):
        check = self.c.get('/mis-citas/')
        self.assertTrue(check.status_code == 302)

    def test_nuestros_medicos(self):
        check = self.c.get('/nuestros-medicos/')
        self.assertTrue(check.status_code == 200)

    def test_salir(self):
        check = self.c.get('/salir/')
        self.assertTrue(check.status_code == 302)
