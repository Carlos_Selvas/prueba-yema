from django.http.response import HttpResponse, Http404
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import login, logout, authenticate
from django.utils.translation import ugettext_lazy as _

from website.forms import RegisterForm, LoginForm, CitaForm
from website.decorators import requiere_usuario_logueado, requiere_usuario_no_logueado
from hospital.models import Paciente
from pediatria.models import Cita, Medico


def inicio(request):
    """
    Vista para mostrar Homepage

    :param request:
    :return:
    """
    return render(request, 'inicio.html')


@requiere_usuario_logueado
def mis_citas(request):
    """
    Vista para mostrar el listado de citas que el paciente ha agendado

    :param request:
    :return:
    """
    paciente = Paciente.objects.get(usuario=request.user)
    citas = Cita.objects.filter(paciente=paciente)
    return render(request, 'mis_citas.html', {'citas': citas})


@requiere_usuario_no_logueado
def accede(request):
    """
    Vista para mostrar formulario de login

    :param request:
    :return:
    """
    # TODO - pruebas unitarias
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            # Loguinea al usuario
            form = form.clean()
            usuario = authenticate(request, username=form['email'], password=form['password'])
            if usuario is not None:
                login(request, usuario)
                return redirect('/mis-citas/')
            else:
                return render(request, 'acceso.html', {'form': LoginForm, 'error': _('Ha ocurrido un error al acceder al sistema. Verifica tus credenciales.')})
    else:
        return render(request, 'acceso.html', {'form': LoginForm})


@requiere_usuario_no_logueado
def registrate(request):
    """
    Vista que muestra formulario de registro

    :param request:
    :return:
    """
    # TODO - pruebas unitarias
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            # TODO: debería revisar si el usuario ya existe

            # Registra al usuario y lo loguinea
            form = form.clean()
            extras = {
                'first_name': form['nombres'],
                'last_name': form['apellidos']
            }
            usuario = User.objects.create_user(form['email'], form['email'], form['password'], **extras)
            login(request, usuario)

            # Hace el registro en la tabla de Paciente
            Paciente.objects.create(usuario=usuario, telefono=form['telefono'])

            # Redirige a Mis citas
            return redirect('/mis-citas/')
    else:
        return render(request, 'registro.html', {'form': RegisterForm})


def nuestros_medicos(request):
    """
    View para el listado y descripción de los médicos

    :param request:
    :return:
    """
    return HttpResponse('nuestros médicos')


def salir(request):
    """
    Desloguea al usuario

    :param request:
    :return: None
    """
    if request.user.is_authenticated:
        logout(request)
    return redirect('/')


@requiere_usuario_logueado
def solicita_cita(request):
    try:
        yo = Paciente.objects.get(usuario=request.user)
    except:
        return Http404
    if request.method == 'POST':
        form = CitaForm(request.POST)
        if form.is_valid():
            form = form.clean()
            medico = Medico.objects.get(id=form['medico'])
            try:
                check = Cita.objects.create(medico=medico, paciente=yo, comentario=form['comentario'],
                                            inicio_cita=form['fecha_hora'])
            except:
                return render(request, 'solicita_cita.html', {'form': CitaForm, 'error': _(
                    'Este médico no está disponible en el horario seleccionado, por favor intenta con uno distinto.')})
            return redirect('/mis-citas/')
    else:
        return render(request, 'solicita_cita.html', {'form': CitaForm})
