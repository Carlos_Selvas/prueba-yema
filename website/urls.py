from django.conf.urls import url
from website import views


urlpatterns = [
    url(r'^$', views.inicio),
    url(r'^registro/$', views.registrate),
    url(r'^accede/$', views.accede),
    url(r'^nuestros-medicos/', views.nuestros_medicos),
    url(r'^mis-citas/', views.mis_citas),
    url(r'^salir/$', views.salir),
    url(r'^solicitar-cita/$', views.solicita_cita),
]
