from django.shortcuts import redirect


def requiere_usuario_no_logueado(function):
    """
    Usar cuando se quiere privar al usuario loguineado de acceder a páginas de registro o acceso

    :param function:
    :return:
    """
    def wrap(request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('/mis-citas/')
        return function(request, *args, **kwargs)

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap


def requiere_usuario_logueado(function):
    """
    Usar cuando se quiere privar al usuario no loguineado de acceder a páginas restringidas

    :param function:
    :return:
    """
    def wrap(request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect('/accede/')
        return function(request, *args, **kwargs)

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap
