from django import forms
from pediatria.funciones_auxiliares import medicos_tuple
from django.utils.translation import ugettext_lazy as _


class RegisterForm(forms.Form):
    nombres = forms.CharField(label='Nombres', max_length=100, required=True, widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'nombres'}))
    apellidos = forms.CharField(label='Apellidos', max_length=100, required=True, widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'apellidos'}))
    email = forms.EmailField(label='Email', max_length=50, required=True, widget=forms.EmailInput(attrs={'class': 'form-control', 'id': 'email'}))
    telefono = forms.CharField(label='Teléfono', max_length=15, required=False, widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'telefono'}))
    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'form-control', 'id': 'password'}))


class LoginForm(forms.Form):
    email = forms.EmailField(label='Email', max_length=50, required=True, widget=forms.EmailInput(attrs={'class': 'form-control', 'id': 'nombres'}))
    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'form-control', 'id': 'password'}), required=True)


class CitaForm(forms.Form):
    MEDICOS = medicos_tuple()
    medico = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control', 'id': 'medico'}), choices=MEDICOS,
                              label=_('Médico'), initial='FIXED')
    fecha_hora = forms.DateTimeField(input_formats=['%Y/%m/%d %H:%M'], widget=forms.DateTimeInput(attrs={'class': 'form-control', 'id': 'fecha-y-hora'}),
                              label=_('Fecha y hora'))
    comentario = forms.CharField(label=_('Comentario'),
                                 widget=forms.Textarea(attrs={'class': 'form-control', 'id': 'comentario'}), required=False)
