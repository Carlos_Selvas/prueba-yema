from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import gettext as _


class Paciente(models.Model):
    usuario = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    telefono = models.CharField(max_length=15, verbose_name=str(_('Teléfono')))
    tipo_sangre = models.CharField(max_length=10, null=True, verbose_name=str(_('Tipo de sangre')))

    def __str__(self):
        return self.usuario.first_name + ' ' + self.usuario.last_name
    
    class Meta:
        verbose_name = str(_('Paciente'))
        verbose_name_plural = str(_('Pacientes'))
